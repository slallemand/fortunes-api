from flask import Flask
from fortune import get_random_fortune

app = Flask(__name__)

@app.route('/')
def index():
    return "Hello world !"

@app.route('/fortunes')
def fortunes():
#    generator = Fortunate('fortunes/citation-celebre.com')
#    return (generator())
    fortune=get_random_fortune('fortunes/citation-celebre.com')
    print (type(fortune))
    return(get_random_fortune('fortunes/citation-celebre.com'))

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)

